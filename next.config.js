/** @type {import('next').NextConfig} */
const path = require('path');

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});
module.exports = withBundleAnalyzer({
  compress: true,
  reactStrictMode: false,
  env: {
    customKey: 'my-value',
    SERVER_URL: process.env.SERVER_URL
  },
  images: {
    domains: ['img.miraebiz.co.kr'],
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  staticPageGenerationTimeout: 90,
})
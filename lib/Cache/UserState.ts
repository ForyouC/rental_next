import { InMemoryCache, makeVar } from '@apollo/client';
import { UserStateReturn } from 'types/generated/graphql';

export type UserStateType = {
  Login: Boolean
  Info?: UserStateReturn
}

export const userStateVar = makeVar<UserStateType>({ Login: false});
import { gql } from "@apollo/client";
import client from "lib/graphql/apollo-client";

export const AgentListGraphql = async (context:any) => {
  if(!context.query.id) {
    return {
      props: {
        Agents: null
      }
    }
  }

  const Cookie = context.req.headers.cookie;

  const { data } = await client.query({
    context: { headers: { Cookie } },
    query: gql`
      query($args: ArgsType!){
        agent(args: $args){
          DataList {
            AIdx
            AName
            RID
          }
        }
      }
    `,
    variables:{
      search: [
        {
          filed: "AIdx",
          operator: "eq",
          value: {
            int: Number(context.query.id)
          }
        }
      ]
    },
  });

  return {
    props: {
      Agents: data.Agents.DataList,
    },
  };
}


export const AgentGqlQuery = gql`
  query AgentsQuery($args: ArgsType!){
    agent(args: $args){
      DataList {
        AIdx
        AName
        RID
        Pwd
      }
      Count
    }
  }
`;
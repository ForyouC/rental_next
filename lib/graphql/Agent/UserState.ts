import { gql } from "@apollo/client";
import client from "lib/graphql/apollo-client";

export const UserStateGqlQuery = gql`
  query UserState{
    userState{
      AIdx
      AName
      Company
      Permsn
      GroupID
      Cms
      Rank
    }
  }
`;
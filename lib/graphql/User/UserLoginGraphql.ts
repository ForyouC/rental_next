import { ReactiveVar } from "@apollo/client";
import gql from "graphql-tag";
import { semanticInputValue } from "lib/Functions/Common";
import { UserStateType } from "lib/Cache/UserState";
import { NextRouter, useRouter } from "next/router";
import { FormEvent, RefObject } from "react";
import Input from "semantic-ui-react/dist/commonjs/elements/Input/Input";
import { LoginReturn } from "types/generated/graphql";

export const LoginGQL = gql`
  mutation Mutation($loginAgentInput: LoginAgentInput!) {
    agentLogin(loginAgentInput: $loginAgentInput) {
      permission {
        Receipt
        Receipt_Excel
        Account
        Account_Excel
        Partner
        Notice
        Pds
        Commission
        Commission_Excel
        Counsel
        Product
        Authority
        Sms
      }
      result {
        P_Result
        AIdx
        Permsn
        Company
        Cms
        Rank
        GroupID
        AName
      }
    }
  }
`;

export const LoginSubmit = (e:FormEvent, refs:any, Mutation:any, UserState:ReactiveVar<UserStateType>, router:NextRouter) => {
  e.preventDefault();
  if(!semanticInputValue(refs.id) || !semanticInputValue(refs.pw)){
    alert("Please input ID, PW");
    return;
  }
  Mutation({variables: { 
    loginAgentInput: {
      RID: semanticInputValue(refs.id),
      Pwd: semanticInputValue(refs.pw)
    }
  }}).then((res:{data: {agentLogin: LoginReturn}}) => {
    const Data:LoginReturn = res?.data?.agentLogin;

    console.log(Data);
    if(Data){
      if(Data.result?.P_Result && Data.result?.P_Result == "CMS-0000"){
        alert("success");
        router.replace("/");
        // UserState({Login:true, Info:{Id: Data.result?.AName, Rank: Data.result?.Rank}})
      }else{
        alert("failed");
        // UserState({Login:false});
      }
    }else{
      alert("failed");
    }
  });
}
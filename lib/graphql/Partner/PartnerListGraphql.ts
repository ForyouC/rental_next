import { gql } from "@apollo/client";
import client from "lib/graphql/apollo-client";

export const PartnerListGraphql = async (context:any) => {
  const Cookie = context.req.headers.cookie;
  const { data } = await client.query({
    context: { headers: { Cookie } },
    query: gql`
      query Partners($args: ArgsType!){
        partner(args: $args) {
          DataList {
            PIdx
            CompanyName
            RegDate
            GroupID
            Parent
            Rank
            Individual
            B_Role
            Lock
            CCenter
            Commission
            DealRate
            Counsel
            RDS
            CeoName
            CeoEmail
            Cms
            CeoHp
            Sms
          }
          Count
        }
      }
    `,
    variables:{
      search: [],
      option: {
        limit: 100
      }
    },
  });

  return {
    props: {
      Partners: data.Partners.DataList,
      Count: data.Partners.Count
    },
  };
}
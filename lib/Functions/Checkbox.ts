import { Maybe } from "graphql/jsutils/Maybe";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import { Dispatch, useEffect, useState } from "react";
import { checkboxesObjectType, checkboxType, initCheckboxType, setCheckboxType } from "types/checkbox";


export const InitDataCheckbox = (data:any, IdFiledName: string, checkbox:checkboxType, setCheckbox:setCheckboxType) => {
  if(data){
    let Data:checkboxesObjectType[] = [];
    if(data.length === 1){
      Data[data[0][IdFiledName]] = {
        Id: data[0][IdFiledName] ?? 0,
        checked: checkbox.all
      }
    }
    else{
      data.reduce((a:any, b:any, index:number) => {
        if(index === 1){
          Data[a[IdFiledName]] = {
            Id: a[IdFiledName] ?? 0,
            checked: checkbox.all
          }
        }
        Data[b[IdFiledName]] = {
          Id: b[IdFiledName] ?? 0,
          checked: checkbox.all
        }
      });
    }
    setCheckbox((bf) => ({...bf, checkboxes: Data}));
  }
}

export const allCheck = (checkbox:checkboxType, setCheckbox:setCheckboxType) => {
  const setData = {
    all: !checkbox.all,
    checkboxes: checkbox.checkboxes.map((elm)=> {
      return {
        Id: elm.Id,
        checked: !checkbox.all
      }
    })
  };
  setCheckbox(setData);
}

export const oneCheck = (Id: number, checkbox:checkboxType, setCheckbox:setCheckboxType) => {
  const Data = {...checkbox};
  Data.checkboxes[Id].checked = !Data.checkboxes[Id]?.checked 

  setCheckbox(Data);
}

export const InitCheckbox = (DataList:any, DataIdName:string, cnt:number):initCheckboxType => {
  const [checkBox, setCheckBox] = useState<checkboxType>({all: false, checkboxes:[]});

  const [cloneData, setCloneData] = useState<any>();

  useEffect(() => {
    if(DataList){
      if(DataList.length === 0) {
        alert("데이터가 없습니다.");
        setCloneData([]);
        return;
      }
      InitDataCheckbox(DataList, DataIdName, checkBox, setCheckBox);
      setCloneData(DataList.map((elm:any) => {
        return {
          ...elm,
          id: elm[DataIdName]
        }
      }));

    }
  }, [DataList]);// eslint-disable-line react-hooks/exhaustive-deps

  return {checkBox: checkBox, setCheckBox: setCheckBox, cloneData: cloneData, setCloneData: setCloneData, count: cnt};
}
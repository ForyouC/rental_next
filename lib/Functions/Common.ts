import { useQuery } from "@apollo/client";
import { userStateVar } from "lib/Cache/UserState";
import { UserStateGqlQuery } from "lib/graphql/Agent/UserState";
import { isEqual } from "lodash";
import { NextRouter, useRouter } from "next/router";
import { Dispatch, MouseEvent, SetStateAction } from "react";
import { PaginationProps } from "semantic-ui-react";
import { AppContextEnum, AppContextType } from "types/common";
import { UserStateReturn } from "types/generated/graphql";

export const EqulToString = (a:any, b:any) => {
  return a === b ? "true" : "false";
}

export const reorder = (list:any, startIndex:number, endIndex:number) => {
  const result:any = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const semanticInputValue = (ref:any) => {
  return ref.current?.inputRef.current?.value ?? null;
}

export const PageChange = async (e:MouseEvent, Pagedata:PaginationProps, Limit:number, refetch:any, setPage:Dispatch<SetStateAction<number>>
  ) => {
  refetch({
    args: {
      option: {
        offset: (Number(Pagedata.activePage ?? Pagedata.defaultActivePage) - 1) * Limit,
        limit: Limit
      }
    } 
  });

  setPage(Number(Pagedata.activePage ?? Pagedata.defaultActivePage));
}

export const PermissionCheck = (router:NextRouter, appContext: AppContextType, Rank:string[]) => {
  const { data, error } = useQuery<{userState: UserStateReturn}>(UserStateGqlQuery, {fetchPolicy: "no-cache"});

  if(data && data.userState.Rank){
    if(Rank.includes(data.userState.Rank)){
      !isEqual(appContext.state, {Login: true, Info: data.userState}) && appContext.dispatch({type: AppContextEnum.Login, args: {Login: true, Info: data.userState}});
      // userStateVar({Login: true, Info: data.userState});
    }else{
      !isEqual(appContext.state, {Login: false}) && appContext.dispatch({type: AppContextEnum.Logout});
      // userStateVar({Login: false});
      router.replace('/User/Login');
    }
  }

  if(error){
    !isEqual(appContext.state, {Login: false}) && appContext.dispatch({type: AppContextEnum.Logout});
    // userStateVar({Login: false});
    router.replace('/User/Login');
  }

  return userStateVar();
}
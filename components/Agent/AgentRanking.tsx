
import { Agent } from 'types/generated/graphql';
import { Button, Checkbox, Icon, Table } from 'semantic-ui-react';
import { allCheck, oneCheck } from 'lib/Functions/Checkbox';
import { ReactSortable } from 'react-sortablejs';
import MyReactSortable from 'components/ForwardRef/MyReactSortable';
import { useRef, useState } from 'react';
import OnlyMobile from 'components/Responsive/OnlyMobile';
import { initCheckboxType } from 'types/checkbox';

const AgentRanking = ({initCheck}: {initCheck:initCheckboxType}) => {
  const file = useRef<HTMLInputElement>(null);
  const [page, setPage] = useState(1);

  const {checkBox, setCheckBox, cloneData, setCloneData, count} = initCheck;

  const AIdxRef = useRef(null);
 
  return (
    <>
      <Table compact celled style={{height:"500px", overflow:"scroll", display: "block", width: "fit-content", margin:"0 auto 20px"}}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              <Checkbox slider value="all" checked={checkBox.all} onChange={() => {
                allCheck(checkBox, setCheckBox);
              }}/>
            </Table.HeaderCell>
            <Table.HeaderCell>AIdx</Table.HeaderCell>
            <Table.HeaderCell>AName</Table.HeaderCell>
            <Table.HeaderCell>Rid</Table.HeaderCell>
            <Table.HeaderCell>Pwd</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        {
          cloneData && 
          <>
            <ReactSortable multiDrag swap delay={100} tag={MyReactSortable} list={cloneData} setList={setCloneData}>
            {cloneData.map((agent:Agent, index:number) => (
              <Table.Row key={agent.AIdx}>
                <Table.Cell collapsing>
                  <Checkbox slider value={agent.AIdx ?? 0} 
                    checked={checkBox.checkboxes[agent.AIdx ?? 0]?.checked}  onChange={() => {oneCheck(agent.AIdx ?? 0, checkBox, setCheckBox);}}
                  />
                </Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>AIdx</OnlyMobile>{agent.AIdx}</Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>AName</OnlyMobile>{agent.AName}</Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>Rid</OnlyMobile>{agent.RID}</Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>Pwd</OnlyMobile>{agent.Pwd}</Table.Cell>
              </Table.Row>
              ))}
            </ReactSortable>
            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell />
                <Table.HeaderCell colSpan='19'>
                  <Button
                    floated='right'
                    icon
                    labelPosition='left'
                    primary
                    size='large'
                  >
                    <Icon name="user" />
                  </Button>
                  <Button size='small'>Approve</Button>
                  <Button disabled size='small'>
                    Approve All
                  </Button>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </>
        }
      </Table>
    </>
  )
}

export default AgentRanking

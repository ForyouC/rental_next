
import { ApolloQueryResult, gql, OperationVariables, useQuery } from '@apollo/client';
import { Agent, AgentReturn, ArgsType, SearchOperatorEnum } from 'types/generated/graphql';
import { Button, Checkbox, Form, Icon, Input, Pagination, Select, Table, TextArea } from 'semantic-ui-react';
import { allCheck, InitCheckbox, oneCheck } from 'lib/Functions/Checkbox';
import { ReactSortable } from 'react-sortablejs';
import MyReactSortable from 'components/ForwardRef/MyReactSortable';
import { PageChange, semanticInputValue } from 'lib/Functions/Common';
import { NextRouter, useRouter } from 'next/router';
import { useRef, useState } from 'react';
import OnlyMobile from 'components/Responsive/OnlyMobile';
import NotMobile from 'components/Responsive/NotMobile';
import { initCheckboxType } from 'types/checkbox';

const genderOptions = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
  { key: 'o', text: 'Other', value: 'other' },
]

const AgentTable = ({initCheck, refetch}: {initCheck:initCheckboxType, refetch:(variables?: Partial<OperationVariables> | undefined) => Promise<ApolloQueryResult<{
  agent: AgentReturn;
}>>}) => {
  const [page, setPage] = useState(1);

  const {checkBox, setCheckBox, cloneData, setCloneData, count} = initCheck;

  const [formData, setFormData] = useState<{
    AIdx: number,
    Email: string
  }>({AIdx: 0, Email: ""});

  const Search = async (e:any) => {
    let variables:{args: ArgsType} = {
      args: {
        search: [],
        option:{
          offset: 0,
          limit: 20
        }
      }
    }
    if(formData?.AIdx){
      variables.args.search?.push(
        {
          filed: "AIdx",
          value: {
            int: formData.AIdx
          },
          operator: SearchOperatorEnum.Eq
        }
      );
    }
    
    setPage(1);
    refetch(variables);
  }
 
  return (
    <>
      <NotMobile style={{width: "auto"}}>
        <Form>
          <Form.Group widths='equal'>
            <Form.Field>
              <label>First Name</label>
              <Input value={formData?.AIdx} onChange={e => setFormData((Pre) => ({...Pre, AIdx: Number(e.target.value)}))} type='number' />
            </Form.Field>
            <Form.Field>
              <label>First Name</label>
              <Select options={genderOptions} placeholder='Gender'/>
            </Form.Field>
          </Form.Group>
          <Form.Field
            id='form-textarea-control-opinion'
            control={TextArea}
            label='Opinion'
            placeholder='Opinion'
          />
          <Form.Field
            id='form-input-control-error-email'
            control={Input}
            label='Email'
            placeholder='joe@schmoe.com'
            error={{
              content: 'Please enter a valid email address',
              pointing: 'below',
            }}
          >
            <Input value={formData?.Email} onChange={e => setFormData((Pre) => ({...Pre, Email: e.target.value}))} type='text' />
          </Form.Field>
          <Form.Field>
            <label>Submit!</label>
            <Button type="button" content="Submit" onClick={(e:any)=>Search(e)} />
          </Form.Field>
        </Form>
      </NotMobile>
      <Table compact celled style={{height:"500px", overflow:"scroll", display: "block", width: "fit-content", margin:"0 auto 20px"}}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              <Checkbox slider value="all" checked={checkBox.all} onChange={() => {
                allCheck(checkBox, setCheckBox);
              }}/>
            </Table.HeaderCell>
            <Table.HeaderCell>AIdx</Table.HeaderCell>
            <Table.HeaderCell>AName</Table.HeaderCell>
            <Table.HeaderCell>Rid</Table.HeaderCell>
            <Table.HeaderCell>Pwd</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        {
          cloneData && 
          <>
            <ReactSortable multiDrag swap delay={100} tag={MyReactSortable} list={cloneData} setList={setCloneData}>
            {cloneData.map((agent:Agent, index:number) => (
              <Table.Row key={agent.AIdx}>
                <Table.Cell collapsing>
                  <Checkbox slider value={agent.AIdx ?? 0} 
                    checked={checkBox.checkboxes[agent.AIdx ?? 0]?.checked}  onChange={() => {oneCheck(agent.AIdx ?? 0, checkBox, setCheckBox);}}
                  />
                </Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>AIdx</OnlyMobile>{agent.AIdx}</Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>AName</OnlyMobile>{agent.AName}</Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>Rid</OnlyMobile>{agent.RID}</Table.Cell>
                <Table.Cell><OnlyMobile inline bold label>Pwd</OnlyMobile>{agent.Pwd}</Table.Cell>
              </Table.Row>
              ))}
            </ReactSortable>
            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell />
                <Table.HeaderCell colSpan='19'>
                  <Button
                    floated='right'
                    icon
                    labelPosition='left'
                    primary
                    size='large'
                  >
                    <Icon name="user" />
                  </Button>
                  <Button size='small'>Approve</Button>
                  <Button disabled size='small'>
                    Approve All
                  </Button>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </>
        }
      </Table>
      {
        cloneData && 
        <>
          <OnlyMobile>
            <Pagination 
              defaultActivePage={1}
              activePage={page}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              prevItem={{ content: <Icon name='angle left' />, icon: true }}
              nextItem={{ content: <Icon name='angle right' />, icon: true }}
              onPageChange={(e, data) => PageChange(e, data, 20, refetch, setPage)}
              boundaryRange={1}
              siblingRange={1}
              totalPages={Math.ceil((count ?? 0) / 20)} 
              size={"tiny"}
            />
          </OnlyMobile>
          <NotMobile>
            <Pagination 
              defaultActivePage={1}
              activePage={page}
              ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
              firstItem={{ content: <Icon name='angle double left' />, icon: true }}
              lastItem={{ content: <Icon name='angle double right' />, icon: true }}
              prevItem={{ content: <Icon name='angle left' />, icon: true }}
              nextItem={{ content: <Icon name='angle right' />, icon: true }}
              onPageChange={(e, data) => PageChange(e, data, 20, refetch, setPage)}
              totalPages={Math.ceil((count ?? 0) / 20)} 
            />
          </NotMobile>
        </>
      }
    </>
  )
}

export default AgentTable

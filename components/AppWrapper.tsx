import { createContext, Dispatch, useContext, useReducer } from 'react';
import { AppContextActionType, AppContextType, UserStateType } from 'types/common';

function reducer(state: UserStateType, action: AppContextActionType) {
  const Action = {
    Logout: {Login: false},
    Login: action.args,
    default: () => {
      throw new Error;
    } 
  }
  return Action[action.type] ?? Action.default();
}

export const AppContext = createContext({} as AppContextType);

export function AppWrapper({ children }:any) {
  const [state, dispatch] = useReducer(reducer, {Login: false});
  return (
    <AppContext.Provider value={{state, dispatch}}>
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
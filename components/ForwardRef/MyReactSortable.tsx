import { forwardRef } from "react";
import Sass from "@/styles/Partner/PartnerList.module.scss";

const MyReactSortable = forwardRef<HTMLDivElement, any>((props, ref) => {
  return <div ref={ref} className={Sass.tbody}>{props.children}</div>;
});
MyReactSortable.displayName= "MyReactSortable";

export default MyReactSortable;
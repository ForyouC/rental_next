import { useRouter } from 'next/router';
import { Button, Checkbox, Icon, Pagination, Table } from 'semantic-ui-react';
import { allCheck, InitCheckbox, oneCheck } from 'lib/Functions/Checkbox';
import { ReactSortable } from "react-sortablejs";
import { Partner, PartnerReturn } from 'types/generated/graphql';
import MyReactSortable from 'components/ForwardRef/MyReactSortable';
import { gql, useQuery } from '@apollo/client';
import { PageChange } from 'lib/Functions/Common';
import OnlyMobile from 'components/Responsive/OnlyMobile';
import NotMobile from 'components/Responsive/NotMobile';
import { useState } from 'react';
import { initCheckboxType } from 'types/checkbox';

export const partnerQuery = gql`
  query Partners($args: ArgsType!){
    partner(args: $args) {
      DataList {
        PIdx
        CompanyName
        RegDate
        GroupID
        Parent
        Rank
        Individual
        B_Role
        Lock
        CCenter
        Commission
        DealRate
        Counsel
        RDS
        CeoName
        CeoEmail
        Cms
        CeoHp
        Sms
      }
      Count
    }
  }
`;

export default function PartnerTable({initCheck, refetch}: {initCheck:initCheckboxType, refetch:() => any}) {
  const router = useRouter();
  const [page, setPage] = useState(1);

  const {checkBox, setCheckBox, cloneData, setCloneData, count} = initCheck;

  return (
    <>
      <Table compact celled style={{height:"500px", overflow:"scroll", display: "block", margin:"0 auto 20px"}}>
        <Table.Header>  
          {/* style={{position:"sticky", top:0, zIndex: 5}} */}
          <Table.Row>
            <Table.HeaderCell>
              <Checkbox slider value="all" checked={checkBox.all} onChange={() => {
                allCheck(checkBox, setCheckBox);
              }}/>
            </Table.HeaderCell>
            <Table.HeaderCell>PIdx</Table.HeaderCell>
            <Table.HeaderCell>CompanyName</Table.HeaderCell>
            <Table.HeaderCell>GroupID</Table.HeaderCell>
            <Table.HeaderCell>Parent</Table.HeaderCell>
            <Table.HeaderCell>Rank</Table.HeaderCell>
            <Table.HeaderCell>Individual</Table.HeaderCell>
            <Table.HeaderCell>B_Role</Table.HeaderCell>
            <Table.HeaderCell>Lock</Table.HeaderCell>
            <Table.HeaderCell>CCenter</Table.HeaderCell>
            <Table.HeaderCell>Commission</Table.HeaderCell>
            <Table.HeaderCell>DealRate</Table.HeaderCell>
            <Table.HeaderCell>Counsel</Table.HeaderCell>
            <Table.HeaderCell>RDS</Table.HeaderCell>
            <Table.HeaderCell>CeoName</Table.HeaderCell>
            <Table.HeaderCell>CeoEmail</Table.HeaderCell>
            <Table.HeaderCell>Cms</Table.HeaderCell>
            <Table.HeaderCell>CeoHp</Table.HeaderCell>
            <Table.HeaderCell>Sms</Table.HeaderCell>
            <Table.HeaderCell>RegDate</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        {
          cloneData && 
          <>
          <ReactSortable multiDrag swap delay={100} tag={MyReactSortable} list={cloneData} setList={setCloneData}>
          {cloneData.map((partner:Partner, index:number) => (
            <Table.Row key={partner.PIdx}>
              <Table.Cell collapsing>
                <Checkbox slider value={partner.PIdx ?? 0} 
                  checked={checkBox.checkboxes[partner.PIdx ?? 0]?.checked}  onChange={() => {oneCheck(partner.PIdx ?? 0, checkBox, setCheckBox);}}
                />
              </Table.Cell>
              <Table.Cell>{partner.PIdx}</Table.Cell>
              <Table.Cell>{partner.CompanyName}</Table.Cell>
              <Table.Cell>{partner.GroupID}</Table.Cell>
              <Table.Cell>{partner.Parent}</Table.Cell>
              <Table.Cell>{partner.Rank}</Table.Cell>
              <Table.Cell>{partner.Individual}</Table.Cell>
              <Table.Cell>{partner.B_Role}</Table.Cell>
              <Table.Cell>{partner.Lock}</Table.Cell>
              <Table.Cell>{partner.CCenter}</Table.Cell>
              <Table.Cell>{partner.Commission}</Table.Cell>
              <Table.Cell>{partner.DealRate}</Table.Cell>
              <Table.Cell>{partner.Counsel}</Table.Cell>
              <Table.Cell>{partner.RDS}</Table.Cell>
              <Table.Cell>{partner.CeoName}</Table.Cell>
              <Table.Cell>{partner.CeoEmail}</Table.Cell>
              <Table.Cell>{partner.Cms}</Table.Cell>
              <Table.Cell>{partner.CeoHp}</Table.Cell>
              <Table.Cell>{partner.Sms}</Table.Cell>
              <Table.Cell>{partner.RegDate}</Table.Cell>
            </Table.Row>
            ))}
            </ReactSortable>
            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell />
                <Table.HeaderCell colSpan='19'>
                  <Button size='small'>Approve</Button>
                  <Button disabled size='small'>
                    Approve All
                  </Button>
                  <Button
                    floated='right'
                    icon
                    labelPosition='left'
                    primary
                    size='small'
                  >
                    Submit
                  </Button>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </>
        }
      </Table>
      {
        cloneData && 
        <>
          <OnlyMobile>
            <Pagination 
              defaultActivePage={1}
              activePage={page}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              prevItem={{ content: <Icon name='angle left' />, icon: true }}
              nextItem={{ content: <Icon name='angle right' />, icon: true }}
              onPageChange={(e, data) => PageChange(e, data, 20, refetch, setPage)}
              boundaryRange={1}
              siblingRange={1}
              totalPages={Math.ceil((count ?? 0) / 100)}
              size={"tiny"} 
            />
          </OnlyMobile>
          <NotMobile>
            <Pagination 
              defaultActivePage={1}
              activePage={page}
              ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
              firstItem={{ content: <Icon name='angle double left' />, icon: true }}
              lastItem={{ content: <Icon name='angle double right' />, icon: true }}
              prevItem={{ content: <Icon name='angle left' />, icon: true }}
              nextItem={{ content: <Icon name='angle right' />, icon: true }}
              onPageChange={(e, data) => PageChange(e, data, 100, refetch, setPage)}
              totalPages={Math.ceil((count ?? 0) / 100)} 
            />
          </NotMobile>
        </>
      }
    </>
  )
}
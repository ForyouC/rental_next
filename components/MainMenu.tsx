import Link from 'next/link';
import React, { useState } from 'react'
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react'
import { useRouter } from 'next/router';
import { FaSass } from 'react-icons/fa';
import Sass from '@/styles/Components/MainMenu.module.scss';
import classNames from 'classnames';
const MainMenu = () => {
  const {pathname} = useRouter();
  const PathObj:any = {
    "/": "Home",
    "/Excel": "Excel",
    "/Agent/AgentList": "Agent",
    "/Partner/PartnerList": "Partner"
  }

  const [state, setState] = useState({ activeItem: PathObj[pathname] ?? 'Home' });
  const handleItemClick = (e:any, { name }:any) => setState({ activeItem: name })
  const { activeItem } = state;
  const [toggle, setToggle] = useState(false);

  return (
    <>
      <Segment inverted className={Sass.FixedMenu} >
        <Menu inverted pointing secondary>
          <Menu.Item
            name='home'
            icon="sidebar"
            style={{padding: "0", margin: "0 0 0 10px"}}
            onClick={() => setToggle(true)}
          >
            <Icon name="sidebar" size="big"/>
          </Menu.Item>
          <Menu.Item
            name='home'
            icon="sidebar"
            position="right"
            style={{padding: "0", margin: "0  0 0"}}
          >
            <Link href={"/User/Login"}>
              <Icon name="user circle" size="big"/>
            </Link>
          </Menu.Item>
        </Menu>
      </Segment>
      <div className={classNames(toggle && Sass.Dimmed)} onClick={() => setToggle(false)} />
      <Menu inverted vertical fixed="left" className={Sass.Menu} style={{ transform: toggle ? "translateX(0)" : "translateX(-100%)"}}>
        <Link href={"/"}>
          <Menu.Item
            name='Home'
            active={activeItem === 'Home'}
          />
        </Link>
        <Link href={"/Cate"}>
          <Menu.Item
            name='카테고리'
            active={activeItem === '카테고리'}
          />
        </Link>
        <Link href={{ pathname: "/Agent/AgentList", query:{id: 10001}}}>
          <Menu.Item
            name='Agent'
            active={activeItem === 'Agent'}
          />
        </Link>
        <Link href={"/Excel"}>
          <Menu.Item
            name='Excel'
            active={activeItem === 'Excel'}
          />
        </Link>
        <Dropdown item 
          text='Partner' 
          name='Partner'
          active={activeItem === 'Partner'}
          onClick={handleItemClick}
        >
          <Dropdown.Menu>
            <Link href={"/Partner/PartnerList"}>
              <Dropdown.Item text='PartnerList' />
            </Link>
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown item 
          text='Setting' 
          name='Setting'
          active={activeItem === 'Setting'}
          onClick={handleItemClick}
        >
          <Dropdown.Menu>
            <Link href={"/Setting/PartnerInfo"}>
              <Dropdown.Item text='PartnerInfo' />
            </Link>
            <Link href={"/Setting/Ranking"}>
              <Dropdown.Item text='Ranking' />
            </Link>
          </Dropdown.Menu>
        </Dropdown>
      </Menu>
    </>
  )
}

export default MainMenu;

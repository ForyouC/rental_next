import MainMenu from 'components/MainMenu';
import { Grid, Segment } from 'semantic-ui-react';
import Sass from '@/styles/Components/Layout/MainLayout.module.scss';
import { useQuery } from '@apollo/client';
import { UserStateGqlQuery } from 'lib/graphql/Agent/UserState';
import { UserStateReturn } from 'types/generated/graphql';
import { useRouter  } from 'next/router';
import { userStateVar } from 'lib/Cache/UserState';
const MainLayout = ({children}:any) => { 
  return (
    <>
      <MainMenu />
      <Segment padded className={Sass.Segment}>
        {children}
      </Segment>
    </>
  )
}

export default MainLayout;
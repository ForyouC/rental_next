import Sass from '@/styles/Components/Responsive/OnlyMobile.module.scss';
import classNames from 'classnames';
import { Label } from 'semantic-ui-react';

const OnlyMobile = ({children, inline = false, bold = false, label = false}:{
  children: any, 
  inline?:boolean, 
  bold?:boolean, 
  label?:boolean
}) => {
  return (

    <div className={classNames([Sass.Div],
      inline && Sass.Inline,
      bold && Sass.Bold
    )}>
      {label ?
      <Label basic pointing='right'>
        {children}
      </Label>
      :
        children
      }
    </div>
  )
}

export default OnlyMobile;
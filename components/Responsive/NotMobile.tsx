import Sass from '@/styles/Components/Responsive/NotMobile.module.scss';
import classNames from 'classnames';
import { CSSProperties } from 'react';
import { Label } from 'semantic-ui-react';

const NotMobile = ({children, inline = false, bold = false, label = false, className = "", style = {}}:{
  children: any, 
  inline?: boolean, 
  bold?: boolean, 
  label?: boolean,
  className?: string,
  style?: CSSProperties
}) => {
  return (

    <div 
      className={classNames([Sass.Div, className],
        inline && Sass.Inline,
        bold && Sass.Bold
      )}
      style={style}
    >
      {label ?
      <Label basic pointing='right'>
        {children}
      </Label>
      :
        children
      }
    </div>
  )
}

export default NotMobile;
import { Header, Icon } from 'semantic-ui-react';
import MainLayout from 'components/Layout/MainLayout';
import AgentTable from 'components/Agent/AgentTable';
import { useQuery } from '@apollo/client';
import { InitCheckbox } from 'lib/Functions/Checkbox';
import { AgentReturn } from 'types/generated/graphql';
import { AgentGqlQuery } from 'lib/graphql/Agent/AgentGraphql';
import { useRouter } from 'next/router';
import { PermissionCheck } from 'lib/Functions/Common';
import { userStateVar } from 'lib/Cache/UserState';
import { useAppContext } from 'components/AppWrapper';

const Home = () => {
  const router = useRouter();
  const appContext = useAppContext();
  PermissionCheck(router, appContext, ["RC"]);

  const { data: agentData, refetch: agentRefetch, error } = useQuery<{agent: AgentReturn}>(AgentGqlQuery, {variables:{args:{}}});
  const agentInitCheck = InitCheckbox(agentData?.agent.DataList, "AIdx", agentData?.agent.Count ?? 0);

  if(error) alert(error.message);
  return (
    appContext.state.Login &&
    <MainLayout>
      <Header as='h2'>
        <Icon name="handshake" style={{display: "block"}} />
        <Header.Content>
          Agent List
          <Header.Subheader>Manage your preferences</Header.Subheader>
        </Header.Content>
      </Header>
      <AgentTable initCheck={agentInitCheck} refetch={agentRefetch}/>
    </MainLayout>
  )
}

export default Home

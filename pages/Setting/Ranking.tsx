import { Form, Header, Icon, Item, Segment } from 'semantic-ui-react';
import MainLayout from 'components/Layout/MainLayout';
import AgentTable from 'components/Agent/AgentTable';
import { useQuery } from '@apollo/client';
import { Agent, AgentReturn } from 'types/generated/graphql';
import { InitCheckbox } from 'lib/Functions/Checkbox';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import AgentRanking from 'components/Agent/AgentRanking';
import { dummyDataTemp } from 'lib/DummyData';
import { AgentGqlQuery } from 'lib/graphql/Agent/AgentGraphql';

const Home = () => {
  const { data: agentData, refetch: agentRefetch } = useQuery<{agent: AgentReturn}>(AgentGqlQuery, {variables:{args:{}}});
  const agentInitCheck = InitCheckbox(agentData?.agent.DataList, "AIdx", agentData?.agent.Count ?? 0);
  const {checkBox: agentCheckbox,cloneData: agentCloneData} = agentInitCheck;

  const [dummyData, setDummyData] = useState(dummyDataTemp);


  const dummyInitCheck = InitCheckbox(dummyData, "AIdx", dummyData?.length);

  const AddRankingItem = () => {
    const Data:any[] = []; 
    agentCheckbox.checkboxes.forEach((item) => {
      if(item.checked) {
        Data.push(item.Id);
      }
    });

    const FilterData = agentCloneData.filter((item:Agent) => {
      if(Data.find(elm => elm === item.AIdx)){
        return true;
      }else{
        return false;
      }
    })

    let duplicateIds:any[] = [];
    
    const LastData = FilterData.filter((item:Agent) => {
      if(dummyData?.find(elm => elm.AIdx === item.AIdx)){
        duplicateIds.push(item.AIdx);
        return false;
      }else{
        return true;
      }
    });

    if(duplicateIds.length > 0){
      alert(`[${duplicateIds.toString()}] 아이디들이 중복되어서 제외하고 등록되었습니다.`);
    }

    setDummyData(dummyData ? [...LastData, ...dummyData] : LastData);
  }

  const removeRankingItem = () => {
    const Data:any[] = [];
    dummyInitCheck.checkBox.checkboxes.forEach(item => {
      if(item.checked) Data.push(item.Id);
    });
    
    setDummyData(dummyData.filter(item => {
      if(Data.find(elm => elm === item.AIdx)){
        return false;
      }else{
        return true;
      }
    }))
  }

  useEffect(() => {
    console.log(agentCloneData, dummyData)
  }, [dummyData]);


  return (
    <MainLayout>
      <Header as='h2'>
        <Icon name="handshake" style={{display: "block"}} />
        <Header.Content>
          Agent List
          <Header.Subheader>Manage your preferences</Header.Subheader>
        </Header.Content>
      </Header>
      <Form>
        <Form.Group>
          <Form.Field width="7">
            <AgentTable initCheck={agentInitCheck} refetch={agentRefetch}/>
          </Form.Field>
          <Form.Field style={{position: "relative",     minHeight: "200px"}} width="2">
            <Segment style={{margin: "auto", padding: "20px",width: "fit-content", position:"absolute", maxHeight:"126px", top: "0", left: "0", right: "0", bottom: "0"}}>
              <Icon name="arrow alternate circle right" size="big" style={{display: "block", margin: "10px auto"}} onClick={AddRankingItem} />
              <Icon name="arrow alternate circle left" size="big" style={{display: "block", margin: "10px auto"}} onClick={removeRankingItem} />
            </Segment>
          </Form.Field>
          <Form.Field width="7">
            <AgentRanking initCheck={dummyInitCheck} />
          </Form.Field>
        </Form.Group>
      </Form>
    </MainLayout>
  )
}

export default Home


import { Button, Form, Grid, Header, Icon, Input, Label, Segment, Select, TextArea} from 'semantic-ui-react';
import MainLayout from 'components/Layout/MainLayout';

const genderOptions = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
  { key: 'o', text: 'Other', value: 'other' },
]

const PartnerInfo = () => {
  return (
    <MainLayout>
      <Header as='h2' style={{margin:"0 0 40px 0"}}>
        <Icon name="handshake" />
        <Header.Content>
          Agent List
          <Header.Subheader>Manage your preferences</Header.Subheader>
        </Header.Content>
      </Header>
      <Form>
        <Form.Group widths='equal'>
          <Form.Field>
            <label>First Name</label>
            <Input type='text' />
          </Form.Field>
          <Form.Field
            id='form-input-control-last-name'
            control={Input}
            label='Last name'
            placeholder='Last name'
          />
          <Form.Field
            control={Select}
            options={genderOptions}
            label={{ children: 'Gender', htmlFor: 'form-select-control-gender' }}
            placeholder='Gender'
            search
            searchInput={{ id: 'form-select-control-gender' }}
          />
        </Form.Group>
        <Form.Field
          id='form-textarea-control-opinion'
          control={TextArea}
          label='Opinion'
          placeholder='Opinion'
        />
        <Form.Field
          id='form-input-control-error-email'
          control={Input}
          label='Email'
          placeholder='joe@schmoe.com'
          error={{
            content: 'Please enter a valid email address',
            pointing: 'below',
          }}
        />
        <Form.Field
          id='form-button-control-public'
          control={Button}
          content='Confirm'
          label='Label with htmlFor'
        />
      </Form>
    </MainLayout>
  )
}

export default PartnerInfo

import { gql, useQuery } from '@apollo/client';
import MainLayout from 'components/Layout/MainLayout';
import { useRouter } from 'next/router';
import { ChangeEvent, Fragment, useEffect, useRef, useState } from 'react';
import { Agent } from 'types/generated/graphql';

const GET_AGENT = gql`
  query{
    agent(args: {}){
      DataList {
        AIdx
        AName
        RID
        Pwd
      }
      Count
    }
  }
`;

export default function FirstPost() {
  const { loading, error, data } = useQuery<{Agents: Agent[]}>(GET_AGENT, {fetchPolicy: "no-cache"});

  const [checkbox, setCheckbox] = useState<{all: boolean, my_checkes: {AIdx:number, checked: boolean}[]}>({all: false, my_checkes:[]});

  const { query } = useRouter();

  useEffect(() => {
    if(!query) return;
    if(query.all && query.all === "on"){
      checkbox.all = true;
    }
  }, [query]);// eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if(data?.Agents){
      const AIdxs = data?.Agents.map((elm) => {
        return {
          AIdx: elm.AIdx ?? 0,
          checked: checkbox.all
        }
      });
      setCheckbox((bf) => ({...bf, my_checkes: AIdxs}));
    }
  }, [data]);// eslint-disable-line react-hooks/exhaustive-deps

  const allCheck = (e:ChangeEvent) => {
    const setData = {
      all: !checkbox.all,
      my_checkes: checkbox.my_checkes.map((elm)=> {
        return {
          AIdx: elm.AIdx,
          checked: !checkbox.all
        }
      })
    };
    setCheckbox(setData);
  }

  const oneCheck = (e:ChangeEvent, AIdx: number) => {
    const setData = {
      ...checkbox,
      my_checkes: checkbox.my_checkes.map((elm) => {
        if(elm.AIdx === AIdx){
          return {
            AIdx: elm.AIdx,
            checked: !elm.checked
          }
        }else{
          return {
            AIdx: elm.AIdx,
            checked: elm.checked
          }
        }
      })
    }

    setCheckbox(setData);
  }

  if (loading) return (<></>);
  if (error) return (<>{error}</>);
  

  return (
    <MainLayout>
      <div>
        {query.all}
        <input type="checkbox" name="all" checked={checkbox.all} onChange={allCheck}/>
        <ul>
          {data?.Agents.map(({ AIdx, AName, RID, Pwd }) => (
            <li key={AIdx}>
              <input type="checkbox" className="my_check" name="my_check" value={AIdx ?? ""} checked={checkbox.my_checkes.find((elm) => elm.AIdx == AIdx)?.checked} onChange={(e) => oneCheck(e, AIdx ?? 0)} /> {AName} / {RID} / {Pwd}
            </li>
          ))}
        </ul>
      </div>
    </MainLayout>
  )
}
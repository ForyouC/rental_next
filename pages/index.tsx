import { Header, Icon } from 'semantic-ui-react';
import MainLayout from 'components/Layout/MainLayout';
import AgentTable from 'components/Agent/AgentTable';
import { useQuery } from '@apollo/client';
import { AgentReturn } from 'types/generated/graphql';
import { InitCheckbox } from 'lib/Functions/Checkbox';
import { AgentGqlQuery } from 'lib/graphql/Agent/AgentGraphql';
import { PermissionCheck } from 'lib/Functions/Common';
import { useRouter } from 'next/router';
import { userStateVar } from 'lib/Cache/UserState';
import { useEffect } from 'react';
import { useAppContext } from 'components/AppWrapper';

const Home = () => {
  const appContext = useAppContext();
  const router = useRouter();
  PermissionCheck(router, appContext, ["RC"]);

  const { data: agentData, refetch: agentRefetch } = useQuery<{agent: AgentReturn}>(AgentGqlQuery, {
    variables: {
      args: {}
    }
  });

  useEffect(() => {
    console.log(appContext);
  }, [appContext]);

  const agentInitCheck = InitCheckbox(agentData?.agent.DataList, "AIdx", agentData?.agent.Count ?? 0);
  return (
    appContext.state.Login === true &&
    <MainLayout>
      <Header as='h2'>
        <Icon name="handshake" style={{display: "block"}} />
        <Header.Content>
          Agent List
          <Header.Subheader>Manage your preferences</Header.Subheader>
        </Header.Content>
      </Header>
      <AgentTable initCheck={agentInitCheck} refetch={agentRefetch}/>
    </MainLayout>
  )
}

export default Home

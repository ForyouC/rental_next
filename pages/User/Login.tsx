import Sass from '@/styles/User/login.module.scss';
import { FetchResult, useMutation } from '@apollo/client';
import Layout from 'components/Layout/MainLayout';
import gql from 'graphql-tag';
import { LoginGQL, LoginSubmit } from 'lib/graphql/User/UserLoginGraphql';
import { userStateVar } from 'lib/Cache/UserState';
import { useRouter } from 'next/router';
import { useRef } from 'react';
import { Button, Form, Grid, Header, Image, Input, Message, Ref, Segment } from 'semantic-ui-react';
import { LoginReturn } from 'types/generated/graphql';


const UserLogin = () => {
  const [Mutation, { data, loading, error }] = useMutation<LoginReturn>(LoginGQL);

  const refs = {
    id: useRef(null),
    pw: useRef(null),
  }

  const router = useRouter();

  if(error) alert(error.message);

  return (
    <Grid textAlign='center' style={{ width: "80%",  height: '100vh', margin: "auto"}} verticalAlign='middle'>
    <Grid.Column style={{ maxWidth: 450 }}>
      <Header as='h2' color='teal' textAlign='center'>
        <Image src='https://react.semantic-ui.com/logo.png' alt="logo"/> Log-in
      </Header>
      <Form size='large' onSubmit={(e) => {LoginSubmit(e,refs,Mutation,userStateVar, router)}}>
        <Segment stacked>
          <Input ref={refs.id} fluid icon='user' iconPosition='left' placeholder='E-mail address' />
          <Input
            ref={refs.pw}
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Password'
            type='password'
            style={{margin: "10px 0"}}
          />

          <Button color='teal' fluid size='large'>
            Login
          </Button>
        </Segment>
      </Form>
      <Message>
        New to us? <a href='#'>Sign Up</a>
      </Message>
    </Grid.Column>
  </Grid>
  );
}

export default UserLogin;
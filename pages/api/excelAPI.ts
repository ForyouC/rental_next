
import ExcelJS from 'exceljs';

import {NextApiRequest , NextApiResponse} from 'next';

const handler = async (req:NextApiRequest, res:NextApiResponse) => {
  var options = {
      filename: './streamed-workbook.xlsx',
      useStyles: true,
      useSharedStrings: true
  };

  var workbook = new ExcelJS.Workbook();
  var sheet = workbook.addWorksheet('My Sheet');
  sheet.columns = [
      { header: 'Id', key: 'id', width: 10 },
      { header: 'Name', key: 'name', width: 32 },
      { header: 'D.O.B.', key: 'DOB', width: 10 }
  ];

  sheet.addRows(req.body);

  const buffer = await workbook.xlsx.writeBuffer();

  res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  res.setHeader("Content-Disposition", "attachment; filename=test.xlsx");
  res.end(buffer);
  // res.status(200).json(req.body);
};

export default handler;
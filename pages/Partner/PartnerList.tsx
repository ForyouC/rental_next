import { Header, Icon } from 'semantic-ui-react';
import MainLayout from 'components/Layout/MainLayout';
import PartnerTable, { partnerQuery } from 'components/Partner/PartnerTable';
import { InitCheckbox } from 'lib/Functions/Checkbox';
import { useQuery } from '@apollo/client';
import { PartnerReturn } from 'types/generated/graphql';


export default function PartnerList() {

  const {error, data, refetch} = useQuery<{partner:PartnerReturn}>(partnerQuery, {variables:{
    args: {}
  }})

  const initCheckBox = InitCheckbox(data?.partner.DataList, "PIdx", data?.partner.Count ?? 0)
  return (
    <MainLayout>
      <Header as='h2'>
        <Icon name="handshake" size="big" />
        <Header.Content>
          Account Settings
          <Header.Subheader>Manage your preferences</Header.Subheader>
        </Header.Content>
      </Header>
      <PartnerTable initCheck={initCheckBox} refetch={refetch}/>
    </MainLayout>
  )
}
import { useQuery } from '@apollo/client';
import { AgentReturn } from 'types/generated/graphql';
import { InitCheckbox } from 'lib/Functions/Checkbox';
import { AgentGqlQuery } from 'lib/graphql/Agent/AgentGraphql';
import { PermissionCheck } from 'lib/Functions/Common';
import { useRouter } from 'next/router';

const Home = () => {
  const router = useRouter();

  const { data: agentData, refetch: agentRefetch } = useQuery<{Agents: AgentReturn}>(AgentGqlQuery);

  return (
    <>
    </>
  )
}

export default Home

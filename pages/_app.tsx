import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { ApolloProvider, useQuery } from '@apollo/client';
import client from 'lib/graphql/apollo-client';
import Head from 'next/head';
import 'semantic-ui-css/semantic.min.css';
import '@/styles/td.scss';
import { AppWrapper } from 'components/AppWrapper';
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
      </Head>
      <AppWrapper>
        <Component {...pageProps} />
      </AppWrapper>
    </ApolloProvider>
  )
}

export default MyApp

import Layout from 'components/Layout/MainLayout';
import { useState } from 'react';

import ExcelJS from 'exceljs';
import { userStateVar } from 'lib/Cache/UserState';
import { PermissionCheck } from 'lib/Functions/Common';
import { useRouter } from 'next/router';
import { useAppContext } from 'components/AppWrapper';

const Excel = () => {
  const router = useRouter();
  const appContext = useAppContext();
  PermissionCheck(router, appContext, ["RC"]);
  
  const [file, setfile] = useState<Blob>();
  const [files, setFiles] = useState<FileList>();

  const handleChange = (e:any) => {
    console.log(e.target.files);
    setfile(e.target.files[0]);
  }

  const handleImport = () => {
    const wb = new ExcelJS.Workbook();
    const reader = new FileReader()
  
    if(file){
      reader.readAsArrayBuffer(file)
      reader.onload = () => {
        const buffer = reader.result as Buffer;
        wb.xlsx.load(buffer).then(workbook => {
          // console.log(workbook, 'workbook instance');
          workbook.eachSheet((sheet, id) => {
            sheet.eachRow((row, rowIndex) => {
              console.log(row.values, rowIndex);
            });
          })
        })
      }
    }
  }

  const Data = [
    {id: 1, name: 'John Doe', DOB: new Date().toUTCString()},
    {id: 2, name: 'John Doe', DOB: new Date().toLocaleDateString()},
    {id: 3, name: 'John Doe', DOB: new Date().toUTCString()},
    {id: 4, name: 'John Doe', DOB: new Date().toLocaleDateString()}
  ];

  const SendData = () => {
    fetch('/api/excelAPI', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(Data),
    }).then( async (res) => {
      const blob = await res.blob();
      var url = window.URL.createObjectURL(blob);
      var a = document.createElement('a');
      a.href = url;
      a.download = "fileName.xlsx";
      document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
      a.click();    
      a.remove();  //afterwards we remove the element again  
    });
  };

  const sendToServer = () => {
    const formData = new FormData();

    if(files){
      for(var i = 0; i < files?.length; i++){
        // console.log(files[i]);
        formData.append('excelFiles', files[i]);
      }
    }
    
		fetch(
			'https://servertest.miraebiz.co.kr/server/excel/multipleExcel',
			{
				method: 'POST',
				body: formData,
			}
		)
    .then((response) => response.json())
    .then((result) => {
      console.log('Success:', result);
    })
    .catch((error) => {
      console.error('Error:', error);
    });
  }

  return (
    appContext.state.Login &&
      <Layout>
        <input type="file" name="myfile" id="" onChange={handleChange} multiple/>
        <input type="button" value="" onClick={handleImport} />
        <button type="button" value="" onClick={SendData} />

        <input type="file" name="myfile" id="" onChange={(e) => e.target.files && setFiles(e.target.files)} multiple />
        <button type="button" value="SendToServer" onClick={sendToServer} />
      </Layout>
  )
}

export default Excel

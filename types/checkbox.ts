import { Dispatch, SetStateAction } from "react"

export type checkboxType = {
  all: boolean;
  checkboxes: checkboxesObjectType[];
}

export type checkboxesObjectType = {
  Id: number;
  checked: boolean;
}

export type setCheckboxType = Dispatch<SetStateAction<checkboxType>>

export type initCheckboxType = {
  checkBox: checkboxType, 
  setCheckBox: setCheckboxType, 
  cloneData: any, 
  setCloneData: Dispatch<any>, 
  count: number
}
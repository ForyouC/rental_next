import { Dispatch } from "react";
import { UserStateReturn } from "./generated/graphql";

export type UserStateType = {
  Login: Boolean
  Info?: UserStateReturn
}

export type AppContextType = {
  state: UserStateType
  dispatch: Dispatch<AppContextActionType>
}

export enum AppContextEnum {
  Logout = "Logout",
  Login = "Login"
}

export type AppContextActionType = {
  type: AppContextEnum
  args?: UserStateType
}